﻿using System.Collections;
using UnityEngine;

public class Pot : MonoBehaviour
{
    private Animator myAnimator;

    private void Awake()
    {
        myAnimator = GetComponent<Animator>(); 
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        myAnimator.SetTrigger("destroy");
    }

    public void DestroyGameObject()
    {
        Destroy(gameObject);
    }
}
