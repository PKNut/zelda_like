﻿using TMPro;
using UnityEngine;

public class Sign : MonoBehaviour
{
    [SerializeField]
    private string message = "The stairs lead to the street. Wanna finally get out of this lightless basement?";
    [SerializeField]
    private GameObject messageBox = default;


    private TextMeshProUGUI messageTextBox = default;
    private bool playerInRange = false;

    private void Awake()
    {
        messageTextBox = messageBox.GetComponentInChildren<TMPro.TextMeshProUGUI>();
    }

    private void Update()
    {
        if (playerInRange && Input.GetButtonDown("Jump"))
        {
            messageTextBox.text = message;
            messageBox.SetActive(!messageBox.activeSelf);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>())
        {
            playerInRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>())
        {
            playerInRange = false;
            messageBox.SetActive(false);
        }
    }


}
