﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField]
    private Transform target = default;

    [SerializeField]
    private float smoothing = 0.5f;

    [SerializeField]
    Vector2 maxPosition = default;
    [SerializeField]
    Vector2 minPosition = default;

    private void LateUpdate()
    {
        if (transform.position != target.position)
        {
            Vector3 targetPosition = new Vector3(target.position.x, target.position.y, transform.position.z);
            targetPosition.x = Mathf.Clamp(targetPosition.x, minPosition.x, maxPosition.x);
            targetPosition.y = Mathf.Clamp(targetPosition.y, minPosition.y, maxPosition.y);
            transform.position = Vector3.Lerp(transform.position, targetPosition, smoothing);
        }
    }

    private void OnLocationTransition(Location location, Vector2 newPlayerPosition)
    {
        UpdateMovementBorders(location.TopRight, location.BottomLeft);
    }

    private void UpdateMovementBorders(Vector2 topRight, Vector2 bottomLeft)
    {
        float cameraHalfHeight = Camera.main.orthographicSize;
        float cameraHalfWidth = cameraHalfHeight * Camera.main.aspect;

        maxPosition = new Vector2(topRight.x - cameraHalfWidth, topRight.y - cameraHalfHeight);
        minPosition = new Vector2(bottomLeft.x + cameraHalfWidth, bottomLeft.y + cameraHalfHeight);
    }

    private void OnEnable()
    {
        LocationTransition.TransitionDelegate += OnLocationTransition;
        // Needs to be updated first time
        UpdateMovementBorders(maxPosition, minPosition);
    }

    private void OnDisable()
    {
        LocationTransition.TransitionDelegate -= OnLocationTransition;
    }


}
