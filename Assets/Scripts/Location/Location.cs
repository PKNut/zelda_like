﻿using UnityEngine;

public class Location : MonoBehaviour
{
    [Tooltip("Used to display location name on transition.")]
    public string Name = "Change Name please! ;)";

    [Tooltip("Only the starting location should be set to 'true'.")]
    public bool EnabledOnStart = false;

    [Tooltip("The World coordinates for the top right corner.")]
    public Vector2 TopRight;

    [Tooltip("The World coordinates for the bottom left corner.")]
    public Vector2 BottomLeft;

    [Tooltip("Not used yet, ignore.")]
    public AudioClip EntranceSound;

    private void Awake()
    {
        gameObject.SetActive(EnabledOnStart);
    }
}
