﻿using System.Collections;
using TMPro;
using UnityEngine;

public class ShowLocationName : MonoBehaviour
{
    [SerializeField]
    private float timeToShowName = 1.2f;
    [SerializeField]
    private float timeToFadeOut = 1f;

    public TextMeshProUGUI Text;
    private Coroutine currentCoroutine;

    private void OnLocationTransition(Location location, Vector2 newPlayerPosition)
    {
        Text.text = location.Name;
        Text.color = Color.white;
        Text.gameObject.SetActive(true);
        if (currentCoroutine != null)
        {
            StopCoroutine(currentCoroutine);
        }
        currentCoroutine = StartCoroutine(ShowLocationTextAndFadeOut());
    }

    private IEnumerator ShowLocationTextAndFadeOut()
    {
        yield return new WaitForSeconds(timeToShowName);

        float timePassed = 0f;

        while (timePassed < timeToFadeOut)
        {
            Text.color = new Color(Text.color.r, Text.color.g, Text.color.b, 1 - (timePassed / timeToFadeOut));

            timePassed += Time.deltaTime;
            yield return 0;
        }
        Text.gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        LocationTransition.TransitionDelegate += OnLocationTransition;
    }

    private void OnDisable()
    {
        LocationTransition.TransitionDelegate -= OnLocationTransition;
        //if (FindObjectOfType<GameEvents>() != null)
        //{}
    }
}
