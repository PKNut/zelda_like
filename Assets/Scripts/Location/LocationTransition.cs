﻿using System.Collections;
using UnityEngine;

public class LocationTransition : MonoBehaviour
{
    public static event System.Action<Location, Vector2> TransitionDelegate = (x, y) => { };

    private Location fromLocation = default;
    [SerializeField]
    private Location toLocation = default;
    [SerializeField]
    private Vector2 newPlayerPosition = default;

    protected static Coroutine currentCoroutine;

    private void Awake()
    {
        fromLocation = transform.parent.GetComponent<Location>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.GetComponent<PlayerMovement>() != null)
        {
            TransitionDelegate(toLocation, newPlayerPosition);

            if (currentCoroutine != null)
            {
                StopCoroutine(currentCoroutine);
            }
            currentCoroutine = StartCoroutine(SwitchLocations());
        }
    }

    private IEnumerator SwitchLocations()
    {
        toLocation.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        fromLocation.gameObject.SetActive(false);
    }

}
