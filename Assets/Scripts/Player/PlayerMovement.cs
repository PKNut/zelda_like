﻿using System.Collections;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float speed = 1f;

    private Animator myAnimator;
    private PlayerState currentState;
    private Rigidbody2D myRigidbody;
    private Vector3 positionChange;

    private void Awake()
    {
        currentState = PlayerState.walk;
        myRigidbody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        myAnimator.SetFloat("moveY", -1);
    }

    void Update()
    {
        positionChange.x = Input.GetAxisRaw("Horizontal");
        positionChange.y = Input.GetAxisRaw("Vertical");

        if (currentState != PlayerState.attack && Input.GetButtonDown("Fire1"))
        {
            StartCoroutine(PlayAttackAnimation());
        }
        else if (currentState == PlayerState.walk)
        {
            UpdateAnimationAndMove();
        }
    }

    private IEnumerator PlayAttackAnimation()
    {
        myAnimator.SetBool("isAttacking", true);
        currentState = PlayerState.attack;
        yield return null;
        myAnimator.SetBool("isAttacking", false);
        for (int i = 0; i < 20; i++)
        {
            yield return null;
        }

        currentState = PlayerState.walk;

    }

    private void UpdateAnimationAndMove()
    {
        if (positionChange != Vector3.zero)
        {
            MoveCharacter(positionChange);
            myAnimator.SetFloat("moveX", positionChange.x);
            myAnimator.SetFloat("moveY", positionChange.y);
            myAnimator.SetBool("isMoving", true);
        }
        else
        {
            myAnimator.SetBool("isMoving", false);
        }
    }

    private void MoveCharacter(Vector3 positionChange)
    {
        myRigidbody.MovePosition(transform.position + (positionChange.normalized * speed * Time.deltaTime));
    }

    private void OnLocationTransition(Location location, Vector2 newPlayerPosition)
    {
        transform.position = newPlayerPosition;
    }

    private void OnEnable()
    {
        LocationTransition.TransitionDelegate += OnLocationTransition;
    }

    private void OnDisable()
    {
        LocationTransition.TransitionDelegate -= OnLocationTransition;
    }

}

public enum PlayerState
{
    walk,
    attack,
    interact
}